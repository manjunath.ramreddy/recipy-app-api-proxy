# recipy-app-api-proxy

NGinx proxy app for our recipe app API

## usage

###### Environment Variable

* `LISTEN_PORT` = Port to listen on (default:`8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (defaulr: `9000`)
